/** ###################################################################
**     THIS BEAN MODULE IS GENERATED BY THE TOOL. DO NOT MODIFY IT.
**     Filename  : WPLUS.C
**     Project   : Project
**     Processor : MC9S12XS128MAA
**     Beantype  : PWM
**     Version   : Bean 02.207, Driver 01.14, CPU db: 3.00.003
**     Compiler  : CodeWarrior HCS12X C Compiler
**     Date/Time : 5/1/2010, 1:23 PM
**     Abstract  :
**         This bean implements a pulse-width modulation generator
**         that generates signal with variable duty and fixed cycle. 
**     Settings  :
**         Used output pin             : 
**             ----------------------------------------------------
**                Number (on package)  |    Name
**             ----------------------------------------------------
**                       79            |  PP5_KWP5_PWM5
**             ----------------------------------------------------
**
**         Timer name                  : PWM45 [16-bit]
**         Counter                     : PWMCNT45  [784]
**         Mode register               : PWMCTL    [773]
**         Run register                : PWME      [768]
**         Prescaler                   : PWMPRCLK  [771]
**         Compare 1 register          : PWMPER45  [792]
**         Compare 2 register          : PWMDTY45  [800]
**         Flip-flop 1 register        : PWMPOL    [769]
**
**         User handling procedure     : not specified
**
**         Output pin
**
**         Port name                   : P
**         Bit number (in port)        : 5
**         Bit mask of the port        : 32
**         Port data register          : PTP       [600]
**         Port control register       : DDRP      [602]
**
**         Runtime setting period      : none
**         Runtime setting ratio       : calculated
**         Initialization:
**              Aligned                : Left
**              Output level           : high
**              Timer                  : Enabled
**              Event                  : Enabled
**         High speed mode
**             Prescaler               : divide-by-1
**             Clock                   : 3333333 Hz
**           Initial value of            period        pulse width (ratio 7.637%)
**             Xtal ticks              : 314568        24024
**             microseconds            : 19660         1502
**             milliseconds            : 20            2
**             seconds (real)          : 0.0196605     0.0015015
**
**     Contents  :
**         Enable         - byte WPLUS_Enable(void);
**         Disable        - byte WPLUS_Disable(void);
**         SetRatio8      - byte WPLUS_SetRatio8(byte Ratio);
**         SetRatio16     - byte WPLUS_SetRatio16(word Ratio);
**         SetDutyTicks16 - byte WPLUS_SetDutyTicks16(word Ticks);
**         SetDutyTicks32 - byte WPLUS_SetDutyTicks32(dword Ticks);
**         SetDutyUS      - byte WPLUS_SetDutyUS(word Time);
**         SetDutyMS      - byte WPLUS_SetDutyMS(word Time);
**         SetDutyReal    - byte WPLUS_SetDutyReal(float Time);
**         SetValue       - byte WPLUS_SetValue(void);
**         ClrValue       - byte WPLUS_ClrValue(void);
**
**     (c) Copyright UNIS, a.s. 1997-2008
**     UNIS, a.s.
**     Jundrovska 33
**     624 00 Brno
**     Czech Republic
**     http      : www.processorexpert.com
**     mail      : info@processorexpert.com
** ###################################################################*/


/* MODULE WPLUS. */

#include "WPLUS.h"

#pragma DATA_SEG WPLUS_DATA
#pragma CODE_SEG WPLUS_CODE
#pragma MESSAGE DISABLE C5919          /* WARNING C5919: Conversion of floating to unsigned integral */

static bool EnUser;                    /* Enable/Disable device by user */
static word RatioStore;                /* Ratio of L-level to H-level */


/*
** ===================================================================
**     Method      :  HWEnDi (bean PWM)
**
**     Description :
**         Enables or disables the peripheral(s) associated with the bean.
**         The method is called automatically as a part of the Enable and 
**         Disable methods and several internal methods.
**         This method is internal. It is used by Processor Expert only.
** ===================================================================
*/
static void HWEnDi(void)
{
  if (EnUser) {                        /* Enable device? */
    PWME_PWME5 = 1;                    /* Run counter */
  } else {                             /* Disable device? */
    PWME_PWME5 = 0;                    /* Stop counter */
    PWMCNT45 = 0;                      /* Reset counter */
  }
}

/*
** ===================================================================
**     Method      :  SetRatio (bean PWM)
**
**     Description :
**         The method reconfigures the compare and modulo registers of 
**         the peripheral(s) when the speed mode changes. The method is 
**         called automatically as a part of the bean 
**         SetHigh/SetLow/SetSlow methods.
**         This method is internal. It is used by Processor Expert only.
** ===================================================================
*/
static void SetRatio(void)
{
  /* PWMDTY45 = (PWMPER45 * (dword)RatioStore + 32768) / 65535;  Calculate new value according to the given ratio */
  asm {
    LDY    PWMPER45
    LDD    RatioStore
    EMUL
    ADDD   #32768
    EXG    D,Y
    ADCB   #0
    ADCA   #0
    LDX    #65535
    EXG    D,Y
    EDIV
    STY    PWMDTY45
  }
}

/*
** ===================================================================
**     Method      :  WPLUS_Enable (bean PWM)
**
**     Description :
**         This method enables the signal generation 
**     Parameters  : None
**     Returns     :
**         ---        - Error code, possible codes:
**                           ERR_OK - OK
**                           ERR_SPEED - This device does not work in
**                           the active speed mode
** ===================================================================
*/
byte WPLUS_Enable(void)
{
  if (!EnUser) {                       /* Is the device disabled by user? */
    EnUser = TRUE;                     /* If yes then set the flag "device enabled" */
    HWEnDi();                          /* Enable the device */
  }
  return ERR_OK;                       /* OK */
}

/*
** ===================================================================
**     Method      :  WPLUS_Disable (bean PWM)
**
**     Description :
**         Disables the bean - it stops the signal generation and events calling.
**     Parameters  : None
**     Returns     :
**         ---        - Error code, possible codes:
**                           ERR_OK - OK
**                           ERR_SPEED - This device does not work in
**                           the active speed mode
** ===================================================================
*/
byte WPLUS_Disable(void)
{
  if (EnUser) {                        /* Is the device enabled by user? */
    EnUser = FALSE;                    /* If yes then set the flag "device disabled" */
    HWEnDi();                          /* Disable the device */
  }
  return ERR_OK;                       /* OK */
}

/*
** ===================================================================
**     Method      :  WPLUS_SetRatio8 (bean PWM)
**
**     Description :
**         This method sets a new duty-cycle ratio.
**     Parameters  :
**         NAME       - DESCRIPTION
**         Ratio      - Ratio is expressed as an 8-bit unsigned integer
**                      number. 0 - 255 value is proportional
**                      to ratio 0 - 100%
**         Note: Calculated duty depends on the timer possibilities
**               and on the selected period.
**     Returns     :
**         ---        - Error code, possible codes:
**                           ERR_OK - OK
**                           ERR_SPEED - This device does not work in
**                           the active speed mode
** ===================================================================
*/
byte WPLUS_SetRatio8(byte Ratio)
{
  RatioStore = (word)Ratio * 257;      /* Store new value of the ratio */
  SetRatio();                          /* Calculate and set up new appropriate values of the duty and period registers */
  return ERR_OK;                       /* OK */
}

/*
** ===================================================================
**     Method      :  WPLUS_SetRatio16 (bean PWM)
**
**     Description :
**         This method sets a new duty-cycle ratio.
**     Parameters  :
**         NAME       - DESCRIPTION
**         Ratio      - Ratio is expressed as an 16-bit unsigned integer
**                      number. 0 - 65535 value is proportional
**                      to ratio 0 - 100%
**         Note: Calculated duty depends on the timer possibilities
**               and on the selected period.
**     Returns     :
**         ---        - Error code, possible codes:
**                           ERR_OK - OK
**                           ERR_SPEED - This device does not work in
**                           the active speed mode
** ===================================================================
*/
byte WPLUS_SetRatio16(word Ratio)
{
  RatioStore = Ratio;                  /* Store new value of the ratio */
  SetRatio();                          /* Calculate and set up new appropriate values of the duty and period registers */
  return ERR_OK;                       /* OK */
}

/*
** ===================================================================
**     Method      :  WPLUS_SetDutyTicks16 (bean PWM)
**
**     Description :
**         This method sets the new duty value of the output signal.
**         The duty is expressed in main oscillator (usualy External
**         osc., internal osc. if exits and external osc. is disabled)
**         ticks as a 16-bit unsigned integer number.
**     Parameters  :
**         NAME            - DESCRIPTION
**         Ticks           - Duty to set in main oscillator ticks
**                      (0 to 65535 ticks in high speed mode)
**     Returns     :
**         ---             - Error code, possible codes:
**                           ERR_OK - OK
**                           ERR_SPEED - This device does not work in
**                           the active speed mode
**                           ERR_MATH - Overflow during evaluation
**                           ERR_RANGE - Parameter out of range
** ===================================================================
*/
byte WPLUS_SetDutyTicks16(word Ticks)
{
  dlong rtval;                         /* Result of two 32-bit numbers multiplication */

  PE_Timer_LngMul((dword)Ticks,(dword)894798507,&rtval); /* Multiply given value and high speed CPU mode coefficient */
  if (PE_Timer_LngHi4(rtval[0],rtval[1],&RatioStore)) { /* Is the result greater or equal than 65536 ? */
    RatioStore = 65535;                /* If yes then use maximal possible value */
  }
  SetRatio();                          /* Calculate and set up new appropriate values of the duty and period registers */
  return ERR_OK;                       /* OK */
}

/*
** ===================================================================
**     Method      :  WPLUS_SetDutyTicks32 (bean PWM)
**
**     Description :
**         This method sets the new duty value of the output signal.
**         The duty is expressed in main oscillator (usualy External
**         osc., internal osc. if exits and external osc. is disabled)
**         ticks as a 32-bit unsigned integer number.
**     Parameters  :
**         NAME            - DESCRIPTION
**         Ticks           - Duty to set in Xtal ticks
**                      (0 to 314568 ticks in high speed mode)
**     Returns     :
**         ---             - Error code, possible codes:
**                           ERR_OK - OK
**                           ERR_SPEED - This device does not work in
**                           the active speed mode
**                           ERR_MATH - Overflow during evaluation
**                           ERR_RANGE - Parameter out of range
** ===================================================================
*/
byte WPLUS_SetDutyTicks32(dword Ticks)
{
  dlong rtval;                         /* Result of two 32-bit numbers multiplication */

  if (Ticks > 314568) {                /* Is the given value out of range? */
    return ERR_RANGE;                  /* If yes then error */
  }
  PE_Timer_LngMul(Ticks,(dword)894798507,&rtval); /* Multiply given value and high speed CPU mode coefficient */
  if (PE_Timer_LngHi4(rtval[0],rtval[1],&RatioStore)) { /* Is the result greater or equal than 65536 ? */
    RatioStore = 65535;                /* If yes then use maximal possible value */
  }
  SetRatio();                          /* Calculate and set up new appropriate values of the duty and period registers */
  return ERR_OK;                       /* OK */
}

/*
** ===================================================================
**     Method      :  WPLUS_SetDutyUS (bean PWM)
**
**     Description :
**         This method sets the new duty value of the output signal. The
**         duty is expressed in microseconds as a 16-bit unsigned integer
**         number.
**     Parameters  :
**         NAME       - DESCRIPTION
**         Time       - Duty to set [in microseconds]
**                      (0 to 19660 us in high speed mode)
**     Returns     :
**         ---        - Error code, possible codes:
**                           ERR_OK - OK
**                           ERR_SPEED - This device does not work in
**                           the active speed mode
**                           ERR_MATH - Overflow during evaluation
**                           ERR_RANGE - Parameter out of range
** ===================================================================
*/
byte WPLUS_SetDutyUS(word Time)
{
  dlong rtval;                         /* Result of two 32-bit numbers multiplication */

  if (Time > 19660) {                  /* Is the given value out of range? */
    return ERR_RANGE;                  /* If yes then error */
  }
  PE_Timer_LngMul((dword)Time,(dword)55924907,&rtval); /* Multiply given value and high speed CPU mode coefficient */
  if (PE_Timer_LngHi3(rtval[0],rtval[1],&RatioStore)) { /* Is the result greater or equal than 65536 ? */
    RatioStore = 65535;                /* If yes then use maximal possible value */
  }
  SetRatio();                          /* Calculate and set up new appropriate values of the duty and period registers */
  return ERR_OK;                       /* OK */
}

/*
** ===================================================================
**     Method      :  WPLUS_SetDutyMS (bean PWM)
**
**     Description :
**         This method sets the new duty value of the output signal. The
**         duty is expressed in milliseconds as a 16-bit unsigned integer
**         number.
**     Parameters  :
**         NAME       - DESCRIPTION
**         Time       - Duty to set [in milliseconds]
**                      (0 to 20 ms in high speed mode)
**     Returns     :
**         ---        - Error code, possible codes:
**                           ERR_OK - OK
**                           ERR_SPEED - This device does not work in
**                           the active speed mode
**                           ERR_MATH - Overflow during evaluation
**                           ERR_RANGE - Parameter out of range
** ===================================================================
*/
byte WPLUS_SetDutyMS(word Time)
{
  dlong rtval;                         /* Result of two 32-bit numbers multiplication */

  if (Time > 20) {                     /* Is the given value out of range? */
    return ERR_RANGE;                  /* If yes then error */
  }
  PE_Timer_LngMul((dword)Time,(dword)218456667,&rtval); /* Multiply given value and high speed CPU mode coefficient */
  if (PE_Timer_LngHi2(rtval[0],rtval[1],&RatioStore)) { /* Is the result greater or equal than 65536 ? */
    RatioStore = 65535;                /* If yes then use maximal possible value */
  }
  SetRatio();                          /* Calculate and set up new appropriate values of the duty and period registers */
  return ERR_OK;                       /* OK */
}

/*
** ===================================================================
**     Method      :  WPLUS_SetDutyReal (bean PWM)
**
**     Description :
**         This method sets the new duty value of the output signal. The
**         duty is expressed in seconds as a real number.
**     Parameters  :
**         NAME       - DESCRIPTION
**         Time       - Duty to set [in seconds]
**                      (0 to 0.0196605 s in high speed mode)
**     Returns     :
**         ---        - Error code, possible codes:
**                           ERR_OK - OK
**                           ERR_SPEED - This device does not work in
**                           the active speed mode
**                           ERR_MATH - Overflow during evaluation
**                           ERR_RANGE - Parameter out of range
** ===================================================================
*/
byte WPLUS_SetDutyReal(float Time)
{
  dword rtlong;                        /* Result of multiplication */

  if (Time > 0.0196605) {              /* Is the given value out of range? */
    return ERR_RANGE;                  /* If yes then error */
  }
  rtlong = (dword)(Time * 3333384.1967397); /* Multiply given value and high speed CPU mode coefficient */
  if (rtlong > 65535) {                /* Is the result greater than 65535 ? */
    RatioStore = 65535;                /* If yes then use maximal possible value */
  }
  else {
    RatioStore = (word)rtlong;
  }
  SetRatio();                          /* Calculate and set up new appropriate values of the duty and period registers */
  return ERR_OK;                       /* OK */
}

/*
** ===================================================================
**     Method      :  WPLUS_SetValue (bean PWM)
**
**     Description :
**         This method sets (sets to "1" = "High") timer flip-flop
**         output signal level. It allow user to directly set the
**         output pin value (=flip-flop state), and can set the signal
**         polarity. This method only works when the timer is disabled
**         (Disable) otherwise it returns the error code. <ClrValue>
**         and <SetValue> methods are used for setting the initial
**         state.
**     Parameters  : None
**     Returns     :
**         ---             - Error code, possible codes:
**                           ERR_OK - OK
**                           ERR_SPEED - This device does not work in
**                           the active speed mode
**                           ERR_ENABLED - Bean is enabled. Bean must be
**                           disabled (see "Disable method")
** ===================================================================
*/
byte WPLUS_SetValue(void)
{
  if (EnUser) {                        /* Is the device enabled by user? */
    return ERR_ENABLED;                /* If yes then error */
  }
  PWMPOL_PPOL5 = 1;                    /* Set output signal level to high */
  PTP_PTP5 = 1;
  return ERR_OK;                       /* OK */
}

/*
** ===================================================================
**     Method      :  WPLUS_ClrValue (bean PWM)
**
**     Description :
**         This method clears (sets to "0" = "Low") timer flip-flop
**         output signal level. It allow user to directly set the
**         output pin value (=flip-flop state), and can set the signal
**         polarity. This method only works when the timer is disabled
**         (Disable) otherwise it returns the error code. <ClrValue>
**         and <SetValue> methods are used for setting the initial
**         state.
**     Parameters  : None
**     Returns     :
**         ---             - Error code, possible codes:
**                           ERR_OK - OK
**                           ERR_SPEED - This device does not work in
**                           the active speed mode
**                           ERR_ENABLED - Bean is enabled. Bean must be
**                           disabled (see "Disable method")
** ===================================================================
*/
byte WPLUS_ClrValue(void)
{
  if (EnUser) {                        /* Is the device enabled by user? */
    return ERR_ENABLED;                /* If yes then error */
  }
  PWMPOL_PPOL5 = 0;                    /* Set output signal level to high */
  PTP_PTP5 = 0;
  return ERR_OK;                       /* OK */
}

/*
** ===================================================================
**     Method      :  WPLUS_Init (bean PWM)
**
**     Description :
**         Initializes the associated peripheral(s) and the bean's 
**         internal variables. The method is called automatically as a 
**         part of the application initialization code.
**         This method is internal. It is used by Processor Expert only.
** ===================================================================
*/
void WPLUS_Init(void)
{
  /* PWMCNT45: BIT15=0,BIT14=0,BIT13=0,BIT12=0,BIT11=0,BIT10=0,BIT9=0,BIT8=0,BIT7=0,BIT6=0,BIT5=0,BIT4=0,BIT3=0,BIT2=0,BIT1=0,BIT0=0 */
  setReg16(PWMCNT45, 0);               /* Reset Counter */ 
  /* PWMSDN: PWMIF=1,PWMIE=0,PWMRSTRT=0,PWMLVL=0,??=0,PWM7IN=0,PWM7INL=0,PWM7ENA=0 */
  setReg8(PWMSDN, 128);                /* Emergency shutdown feature settings */ 
  RatioStore = 5005;                   /* Store initial value of the ratio */
  EnUser = TRUE;                       /* Enable device */
  /* PWMDTY45: BIT15=0,BIT14=0,BIT13=0,BIT12=1,BIT11=0,BIT10=0,BIT9=1,BIT8=1,BIT7=1,BIT6=0,BIT5=0,BIT4=0,BIT3=1,BIT2=1,BIT1=0,BIT0=1 */
  setReg16(PWMDTY45, 5005);            /* Store initial value to the duty-compare register */ 
  /* PWMPER45: BIT15=1,BIT14=1,BIT13=1,BIT12=1,BIT11=1,BIT10=1,BIT9=1,BIT8=1,BIT7=1,BIT6=1,BIT5=1,BIT4=1,BIT3=1,BIT2=1,BIT1=1,BIT0=1 */
 // setReg16(PWMPER45, 65535);           /* and to the period register */ 
  setReg16(PWMPER45, 40030);           /* and to the period register */ 
  /* PWMPRCLK: ??=0,PCKB2=0,PCKB1=0,PCKB0=0,??=0,PCKA2=0,PCKA1=0,PCKA0=0 */
  setReg8(PWMPRCLK, 0);                /* Set prescaler register */ 
  /* PWMSCLA: BIT7=0,BIT6=0,BIT5=0,BIT4=0,BIT3=0,BIT2=1,BIT1=1,BIT0=0 */
 // setReg8(PWMSCLA, 10);                 /* Set scale register */ 
  setReg8(PWMSCLA, 16);                 /* Set scale register */ 
  /* PWMCLK: PCLK5=1 */
  setReg8Bits(PWMCLK, 32);             /* Select clock source */ 
  HWEnDi();                            /* Enable/disable device according to status flags */
}

/* END WPLUS. */

/*
** ###################################################################
**
**     This file was created by UNIS Processor Expert 3.00 [04.12]
**     for the Freescale HCS12X series of microcontrollers.
**
** ###################################################################
*/
