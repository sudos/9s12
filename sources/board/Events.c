/** ###################################################################
 **     Filename  : Events.C
 **     Project   : Project
 **     Processor : MC9S12XS128MAA
 **     Beantype  : Events
 **     Version   : Driver 01.04
 **     Compiler  : CodeWarrior HCS12X C Compiler
 **     Date/Time : 4/15/2010, 4:42 PM
 **     Abstract  :
 **         This is user's event module.
 **         Put your event handler code here.
 **     Settings  :
 **     Contents  :
 **         IRQ_ISR   - void IRQ_ISR(void);
 **         PJINT_ISR - void PJINT_ISR(void);
 **
 **     (c) Copyright UNIS, a.s. 1997-2008
 **     UNIS, a.s.
 **     Jundrovska 33
 **     624 00 Brno
 **     Czech Republic
 **     http      : www.processorexpert.com
 **     mail      : info@processorexpert.com
 ** ###################################################################*/
/* MODULE Events */


//#include "Cpu.h"
//#include "Events.h"

//#include "Cpu.h"
//#include "Events.h"
#include "Cpu.h"
#include "Events.h"
#include "includes.h"

#pragma CODE_SEG DEFAULT


//byte date[LINE][PIEX];

void PJINT_D(void){
  PJINT_Disable();
}
void IRQ_D(void){
  IRQ_Disable();
}
void PJINT_E(void){
  PJINT_Enable();
}
void IRQ_E(void){
  IRQ_Enable();
}
////////////////////////////////////////////////////////////////////


/*
 ** ===================================================================
 **     Event       :  IRQ_ISR (module Events)
 **
 **     From bean   :  IRQ [ExtInt]
 **     Description :
 **         This event is called when an active signal edge/level has
 **         occurred.
 **     Parameters  : None
 **     Returns     : Nothing
 ** ===================================================================
 */
void IRQ_ISR(void)
{   
        IRQ_S++;
}



/*
 ** ===================================================================
 **     Event       :  PJINT_ISR (module Events)
 **
 **     From bean   :  PJINT [ExtInt]
 **     Description :
 **         This event is called when an active signal edge/level has
 **         occurred.
 **     Parameters  : None
 **     Returns     : Nothing
 ** ===================================================================
 */
void PJINT_ISR(void)
{
        PJINT_S = 0;

}


/*
 ** ===================================================================
 **     Event       :  SPI_OnRxChar (module Events)
 **
 **     From bean   :  SPI [SynchroMaster]
 **     Description :
 **         This event is called after a correct character is received.
 **         The event is available only when the <Interrupt
 **         service/event> property is enabled.
 **     Parameters  : None
 **     Returns     : Nothing
 ** ===================================================================
 */
void SPI_OnRxChar(void)
{
	/* Write your code here ... */
}

/*
 ** ===================================================================
 **     Event       :  SPI_OnTxChar (module Events)
 **
 **     From bean   :  SPI [SynchroMaster]
 **     Description :
 **         This event is called after a character is transmitted.
 **     Parameters  : None
 **     Returns     : Nothing
 ** ===================================================================
 */
void SPI_OnTxChar(void)
{
	/* Write your code here ... */
}

/*
 ** ===================================================================
 **     Event       :  SPI_OnError (module Events)
 **
 **     From bean   :  SPI [SynchroMaster]
 **     Description :
 **         This event is called when a channel error (not the error
 **         returned by a given method) occurs. The errors can be read
 **         using <GetError> method.
 **         The event is available only when the <Interrupt
 **         service/event> property is enabled.
 **     Parameters  : None
 **     Returns     : Nothing
 ** ===================================================================
 */
void SPI_OnError(void)
{
	/* Write your code here ... */
}

/*
 ** ===================================================================
 **     Event       :  Cpu_OnSwINT (module Events)
 **
 **     From bean   :  Cpu [MC9S12XS256_80]
 **     Description :
 **         This software event is called after software reset.
 **     Parameters  : None
 **     Returns     : Nothing
 ** ===================================================================
 */
void Cpu_OnSwINT(void)
{
	/* Write your code here ... */
}

/* END Events */

/*
 ** ###################################################################
 **
 **     This file was created by UNIS Processor Expert 3.00 [04.12]
 **     for the Freescale HCS12X series of microcontrollers.
 **
 ** ###################################################################
 */
