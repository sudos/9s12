#include "image_cap.h"

int IRQ_S = 0;
int PJINT_S = 0;

int image_cap(char image[VER][HOR]){

	char i=0,j=0,k=0;
	int pixel=0;


	IRQ_S=0;
	PJINT_S=1;

	pixel=0;
	DATA_SetDir(FALSE);                     // Set input
	while(PJINT_S);
	PJINT_D();
	IRQ_E();

	//DATA_SetDir(FALSE);                     // Set input
	while(pixel< V_OFFSET) pixel++;
	while(j<VER){

		if(IRQ_S < 2){

			if(IRQ_S == 1){    
				fifo_ren();

				FIFO_RCLK=0;
				FIFO_RCLK=1;
				while(i<HOR){

					FIFO_RCLK = 0;
					FIFO_RCLK = 1;
					image[j][i++]=DATA_GetVal();
				}

				fifo_rs();
				j++;
				i=0;

				while(IRQ_S == 1);
			}else{
				// May do someting 
			}
		}else{
			LED3=~LED3;
			IRQ_S=0;
			while(pixel < H_OFFSET) pixel++;
			FIFO_REN = 1;                   // Close read
			FIFO_OE=1;

			FIFO_WEN = 0;                   // Set device condition to WRITE
			ADC_OE=0;

			pixel=0;
			while(IRQ_S==0);
		}

	}
	IRQ_D();
	PJINT_E();

	FIFO_OE=1;
	ADC_OE=1;

	return 0;

}


int fifo_wen(void){
	FIFO_REN = 1;                   // Close read
	FIFO_OE=1;

	FIFO_WEN = 0;                   // Set device condition to WRITE
	ADC_OE=0;

	return 0;
}

int fifo_ren(void){
	FIFO_WEN = 1;
	ADC_OE=1;

	FIFO_OE=0;
	FIFO_REN = 0;


	return 0;
}

int fifo_rclk(void){
	FIFO_RCLK = 0;
	FIFO_RCLK = 1;

	return 0;
}

int fifo_rs(void){
	FIFO_RS=1;
	FIFO_RS=0;
	FIFO_RS=1;

	return 0;
}
