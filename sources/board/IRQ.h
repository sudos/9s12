/** ###################################################################
**     THIS BEAN MODULE IS GENERATED BY THE TOOL. DO NOT MODIFY IT.
**     Filename  : IRQ.H
**     Project   : Project
**     Processor : MC9S12XS128MAA
**     Beantype  : ExtInt
**     Version   : Bean 02.090, Driver 01.18, CPU db: 3.00.003
**     Compiler  : CodeWarrior HCS12X C Compiler
**     Date/Time : 4/16/2010, 4:23 PM
**     Abstract  :
**         This bean "ExtInt" implements an external 
**         interrupt, its control methods and interrupt/event 
**         handling procedure.
**         The bean uses one pin which generates interrupt on 
**         selected edge.
**     Settings  :
**         Interrupt name              : Virq
**         User handling procedure     : IRQ_ISR
**
**         Used pin                    :
**             ----------------------------------------------------
**                Number (on package)  |    Name
**             ----------------------------------------------------
**                       39            |  PE1_IRQ
**             ----------------------------------------------------
**
**         Port name                   : E
**
**         Bit number (in port)        : 1
**         Bit mask of the port        : 2
**
**         Signal edge/level           : falling
**         Priority                    : 4
**         Pull option                 : off
**         Initial state               : Enabled
**
**         Edge register               : IRQCR     [30]
**         Enable register             : IRQCR     [30]
**
**         Port data register          : PORTE     [8]
**         Port control register       : DDRE      [9]
**         Port function register      : IRQCR     [30]
**     Contents  :
**         Enable  - void IRQ_Enable(void);
**         Disable - void IRQ_Disable(void);
**         GetVal  - bool IRQ_GetVal(void);
**
**     (c) Copyright UNIS, a.s. 1997-2008
**     UNIS, a.s.
**     Jundrovska 33
**     624 00 Brno
**     Czech Republic
**     http      : www.processorexpert.com
**     mail      : info@processorexpert.com
** ###################################################################*/

#ifndef __IRQ_H
#define __IRQ_H

/* MODULE IRQ. */

/*Including shared modules, which are used in the whole project*/
#include "PE_Types.h"
#include "PE_Error.h"
#include "PE_Const.h"
#include "IO_Map.h"
#include "Events.h"
#include "Cpu.h"

#pragma CODE_SEG IRQ_CODE

#define IRQ_Enable() (IRQCR_IRQEN = 1)  /* Enable interrupt */
/*
** ===================================================================
**     Method      :  IRQ_Enable (bean ExtInt)
**
**     Description :
**         Enable the bean - the external events are accepted. This
**         method is enabled only if HW module allows enable/disable of
**         the interrupt.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/

#define IRQ_Disable() (IRQCR_IRQEN = 0) /* Disable interrupt */
/*
** ===================================================================
**     Method      :  IRQ_Disable (bean ExtInt)
**
**     Description :
**         Disable the bean - the external events are not accepted.
**         This method is enabled only if HW module allows
**         enable/disable of the interrupt.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/

#pragma CODE_SEG __NEAR_SEG NON_BANKED
__interrupt void IRQ_Interrupt(void);
#pragma CODE_SEG IRQ_CODE
/*
** ===================================================================
**     Method      :  IRQ_Interrupt (bean ExtInt)
**
**     Description :
**         The method services the interrupt of the selected peripheral(s)
**         and eventually invokes the beans event(s).
**         This method is internal. It is used by Processor Expert only.
** ===================================================================
*/

#define IRQ_GetVal() ((PORTE) & 2)
/*
** ===================================================================
**     Method      :  IRQ_GetVal (bean ExtInt)
**
**     Description :
**         Returns the actual value of the input pin of the bean.
**     Parameters  : None
**     Returns     :
**         ---             - Returned input value. Possible values:
**                           FALSE - logical "0" (Low level)
**                           TRUE - logical "1" (High level)
** ===================================================================
*/

#pragma CODE_SEG DEFAULT

/* END IRQ. */

#endif /* __IRQ_H*/
/*
** ###################################################################
**
**     This file was created by UNIS Processor Expert 3.00 [04.12]
**     for the Freescale HCS12X series of microcontrollers.
**
** ###################################################################
*/
