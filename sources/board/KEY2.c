/** ###################################################################
**     THIS BEAN MODULE IS GENERATED BY THE TOOL. DO NOT MODIFY IT.
**     Filename  : KEY2.C
**     Project   : Project
**     Processor : MC9S12XS128MAA
**     Beantype  : BitIO
**     Version   : Bean 02.072, Driver 03.12, CPU db: 3.00.003
**     Compiler  : CodeWarrior HCS12X C Compiler
**     Date/Time : 4/20/2010, 4:47 PM
**     Abstract  :
**         This bean "BitIO" implements an one-bit input/output.
**         It uses one bit/pin of a port.
**         Note: This bean is set to work in Input direction only.
**         Methods of this bean are mostly implemented as a macros
**         (if supported by target language and compiler).
**     Settings  :
**         Used pin                    :
**             ----------------------------------------------------
**                Number (on package)  |    Name
**             ----------------------------------------------------
**                       26            |  PE5
**             ----------------------------------------------------
**
**         Port name                   : E
**
**         Bit number (in port)        : 5
**         Bit mask of the port        : 32
**
**         Initial direction           : Input (direction cannot be changed)
**         Initial output value        : 0
**         Initial pull option         : off
**
**         Port data register          : PORTE     [8]
**         Port control register       : DDRE      [9]
**         Port function register      : IRQCR     [30]
**
**         Optimization for            : speed
**     Contents  :
**         GetDir - bool KEY2_GetDir(void);
**         GetVal - bool KEY2_GetVal(void);
**
**     (c) Copyright UNIS, a.s. 1997-2008
**     UNIS, a.s.
**     Jundrovska 33
**     624 00 Brno
**     Czech Republic
**     http      : www.processorexpert.com
**     mail      : info@processorexpert.com
** ###################################################################*/

/* MODULE KEY2. */

#include "KEY2.h"
  /* Including shared modules, which are used in the whole project */
#include "PE_Types.h"
#include "PE_Error.h"
#include "PE_Const.h"
#include "IO_Map.h"
#include "Cpu.h"

#pragma DATA_SEG KEY2_DATA
#pragma CODE_SEG KEY2_CODE
#pragma CONST_SEG KEY2_CONST           /* Constant section for this module */
/*
** ===================================================================
**     Method      :  KEY2_GetVal (bean BitIO)
**
**     Description :
**         This method returns an input value.
**           a) direction = Input  : reads the input value from the
**                                   pin and returns it
**           b) direction = Output : returns the last written value
**         Note: This bean is set to work in Input direction only.
**     Parameters  : None
**     Returns     :
**         ---             - Input value. Possible values:
**                           FALSE - logical "0" (Low level)
**                           TRUE - logical "1" (High level)

** ===================================================================
*/
/*
bool KEY2_GetVal(void)

**  This method is implemented as a macro. See KEY2.h file.  **
*/

/*
** ===================================================================
**     Method      :  KEY2_GetDir (bean BitIO)
**
**     Description :
**         This method returns direction of the bean.
**     Parameters  : None
**     Returns     :
**         ---        - Direction of the bean (always FALSE, Input only)
**                      FALSE = Input, TRUE = Output
** ===================================================================
*/
/*
bool KEY2_GetDir(void)

**  This method is implemented as a macro. See KEY2.h file.  **
*/


/* END KEY2. */
/*
** ###################################################################
**
**     This file was created by UNIS Processor Expert 3.00 [04.12]
**     for the Freescale HCS12X series of microcontrollers.
**
** ###################################################################
*/
