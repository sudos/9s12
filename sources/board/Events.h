/** ###################################################################
**     Filename  : Events.H
**     Project   : Project
**     Processor : MC9S12XS128MAA
**     Beantype  : Events
**     Version   : Driver 01.04
**     Compiler  : CodeWarrior HCS12X C Compiler
**     Date/Time : 4/15/2010, 4:42 PM
**     Abstract  :
**         This is user's event module.
**         Put your event handler code here.
**     Settings  :
**     Contents  :
**         IRQ_ISR   - void IRQ_ISR(void);
**         PJINT_ISR - void PJINT_ISR(void);
**
**     (c) Copyright UNIS, a.s. 1997-2008
**     UNIS, a.s.
**     Jundrovska 33
**     624 00 Brno
**     Czech Republic
**     http      : www.processorexpert.com
**     mail      : info@processorexpert.com
** ###################################################################*/

#ifndef __Events_H
#define __Events_H
/* MODULE Events */

#include "PE_Types.h"
#include "PE_Error.h"
#include "PE_Const.h"
#include "IO_Map.h"
#include "PE_Timer.h"
#include "PJINT.h"
#include "IRQ.h"
#include "LED.h"
#include "DATA.h"
#include "ADCCLK.h"
#include "KEY2.h"
#include "WPLUS.h"
#include "ENCDR.h"
#include "EF.h"
#include "SCI.h"
#include "PWM1.h"
#include "PWM2.h"



#define KEY1 PORTE_PE6
#define KEY2 PORTE_PE5
#define KEY3 PORTE_PE4
//#define KEY4 PORTE_PE7

#define D0 PORTA_PA0
#define D1 PORTA_PA1
#define D2 PORTA_PA2
#define D3 PORTA_PA3
#define D4 PORTA_PA4
#define D5 PORTA_PA5
#define D6 PORTA_PA6
#define D7 PORTA_PA7


#pragma CODE_SEG DEFAULT

void IRQ_ISR(void);
/*
** ===================================================================
**     Event       :  IRQ_ISR (module Events)
**
**     From bean   :  IRQ [ExtInt]
**     Description :
**         This event is called when an active signal edge/level has
**         occurred.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
void PJINT_ISR(void);
/*
** ===================================================================
**     Event       :  PJINT_ISR (module Events)
**
**     From bean   :  PJINT [ExtInt]
**     Description :
**         This event is called when an active signal edge/level has
**         occurred.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/

void SPI_OnRxChar(void);
/*
** ===================================================================
**     Event       :  SPI_OnRxChar (module Events)
**
**     From bean   :  SPI [SynchroMaster]
**     Description :
**         This event is called after a correct character is received.
**         The event is available only when the <Interrupt
**         service/event> property is enabled.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/

void SPI_OnTxChar(void);
/*
** ===================================================================
**     Event       :  SPI_OnTxChar (module Events)
**
**     From bean   :  SPI [SynchroMaster]
**     Description :
**         This event is called after a character is transmitted.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/

void SPI_OnError(void);
/*
** ===================================================================
**     Event       :  SPI_OnError (module Events)
**
**     From bean   :  SPI [SynchroMaster]
**     Description :
**         This event is called when a channel error (not the error
**         returned by a given method) occurs. The errors can be read
**         using <GetError> method.
**         The event is available only when the <Interrupt
**         service/event> property is enabled.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/

void Cpu_OnSwINT(void);
/*
** ===================================================================
**     Event       :  Cpu_OnSwINT (module Events)
**
**     From bean   :  Cpu [MC9S12XS256_80]
**     Description :
**         This software event is called after software reset.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/

/* END Events */
#endif /* __Events_H*/

/*
** ###################################################################
**
**     This file was created by UNIS Processor Expert 3.00 [04.12]
**     for the Freescale HCS12X series of microcontrollers.
**
** ###################################################################
*/
