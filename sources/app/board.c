#include "board.h"

/*  Functions implement     */


void fifo_init(void);

void delay(int t){

	int i,j;
	if (t<1)  t=1;
	for (i=0; i<t; i++)
		for(j=0; j<3338; j++);
}

void u_delay(int t){

	int i,j;
	if(t<1) t=1;
	for (i=0; i<t; i++)
		for(j=0; j<50; j++);
}
// Handly PLL
void SetBusCLK_64M(void)
{
	CLKSEL=0X00;//disengage PLL to system
	PLLCTL_PLLON=1;//turn on PLL
	SYNR =0xc0 | 0x07;
	REFDV=0x80 | 0x01;
	POSTDIV=0x00; //pllclock=2*osc*(1+SYNR)/(1+REFDV)=128MHz;
	_asm(nop); //BUS CLOCK=64M
	_asm(nop);
	while(!(CRGFLG_LOCK==1)); //when pll is steady ,then use it;
	CLKSEL_PLLSEL =1; //engage PLL to system;
}

/*  Initialze drivers   */
void dvrinit(void){

	SetBusCLK_64M();
	/* Enable modules  */

	/* Enable 33886 chip MIDS*/
	DDRT_DDRT1=0;
	DDRT_DDRT2=0;
	DDRT_DDRT3=1;
	PTT_PTT3=0;		// 1 enable; 0 disable

	/*  Enable MOTOR control module */
	PWM1_Enable();
	PWM2_Enable();
	WPLUS_Enable();

	/*  Initial MOTOR control module  */
	WPLUS_SetRatio16(4500);
	PWM1_SetRatio16(40000);
	PWM2_SetRatio16(0); 

	fifo_init();
}


void fifo_init(void){
	DDRB_DDRB0 = 1;                 // FIFO_REN
	DDRB_DDRB1 = 1;                 // FIFO_RCLK
	DDRB_DDRB2 = 1;                 // FIFO_OE

	DDRB_DDRB3 = 1;                 // ADC_OE

	DDRT_DDRT4 = 1;                 // FIFO_RS
	DDRT_DDRT5 = 1;                 // FIFO_WEN
	//  DDRT_DDRT6 = 0;                 // FIFO_EF
	DDRM_DDRM0 = 0;                 // ARM_RDY as hotplug
	DDRM_DDRM5 = 1;
	DDRM_DDRM3 = 1;
	DDRM_DDRM2 = 0;                 // ARM_RCVD

	DDRS_DDRS3 = 1;

	DATA_DDR = 0;
	ARM_MODE=0;
	//       ARM_REN=0;
	ARM_CLK=0;

	FIFO_RS=1;
	FIFO_RS = 0;
	FIFO_RS = 1;

	FIFO_WEN = 1;

	FIFO_OE=1;
	FIFO_REN=1;
	FIFO_RCLK=1;

	ADC_OE = 1;
}


// Send image frame data. Debug func
void transchar(char image[VER][HOR]){
	int i,j;

	for(i=0;i<VER;i++){
		for(j=0;j<HOR;j++){
			while(ERR_OK!=SCI_SendChar(image[i][j]));
		}
	}
}
// Send image frame data, Debug func
void transchar_n(char image[VER][HOR]){
	byte i,j;

	for(i=0;i<VER;i++){
		for(j=0;j<HOR;j++){
			while(ERR_OK!=SPI_SendChar(image[i][j]));
		}
	}
}

int send(unsigned char *buf1,unsigned char *buf2,unsigned int len1,unsigned int len2)
{
	unsigned int i=0,r=0,j=0;

	FIFO_OE = 1;                   // Disable FIFO write
	ADC_OE=1;                       // Disable ADC

	if(ARM_RDY==0) return -1;

	DATA_SetDir(TRUE);              // Set output
	/*
	   for(i=0; i<500; i++){
	   DATA_PutVal(255);
	   }
	   */

	ARM_CLK=0;
	ARM_MODE=1;

	//for(j=0;j<4;j++){


	for(j=0; j<VER; j++){

		for (i=0;i<HOR;i++)
		{
			DATA_PutVal(image_data[j][i]);
			//DATA=buf[i];
			r=ARM_RCVD;
			ARM_CLK=~ARM_CLK;
			while(r==ARM_RCVD);
		}
	}

	for (i=0;i<len2;i++)
	{
		DATA_PutVal(buf2[i]);
		//DATA=buf[i];
		r=ARM_RCVD;
		ARM_CLK=~ARM_CLK;
		while(r==ARM_RCVD);
	}
	//}


	DATA_SetDir(FALSE);                     // Set input
	ARM_MODE=0;  
	ARM_CLK=0;


}

int transdbg(char *buff, int size){
	int i=0,j=0;
	int last=0;
	int test=0;

	if(size<=0 || size>2000) return -1;
	FIFO_OE = 1;                   // Disable FIFO write
	ADC_OE=1;                       // Disable ADC

	DATA_SetDir(TRUE);              // Set output
	/*
	   for(i=0; i<500; i++){
	   DATA_PutVal(255);
	   }
	   */
	ARM_MODE=1;



	last=0;
	ARM_CLK=0;
	for(j=0; j<4; j++){

		for(i=0; i<size; i++){
			last= ARM_RCVD;
			DATA_PutVal(*(buff+i));
			test=  ARM_RCVD;
			ARM_CLK=1;

			while(last==ARM_RCVD);
			last=ARM_RCVD;

			ARM_CLK=0;
			while(last==ARM_RCVD);

		}
	}

	DATA_SetDir(FALSE);                     // Set input
	ARM_MODE=0;        
	return size;
}

void sendb(void){
	while(ERR_OK!=SCI_SendChar('b'));

}
