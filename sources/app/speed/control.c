#include "control.h"
#include "image_prc.h"

struct control icontrol;

void motor_ctrl(unsigned int s, int direction){

        if(s<0 || s>65535) s=0;
        
        if(direction==0){
                PWM1_SetRatio16(0);
        	PWM2_SetRatio16(s);
        }else{
                PWM2_SetRatio16(0);
        	PWM1_SetRatio16(s);
        }
}

void wplus_ctrl(unsigned int s){
        
        WPLUS_SetRatio16(s);
}

int control_pad(struct track *p){
        int x,y;
        unsigned int m=S0,w=MID,ms=S0,ws=MID;
        int i=0,j=0;
        int head=0;
        int trail=0;
        
        
        x=p[0].x;
        y=p[0].y;
        
        if(x<15) ws=L0;
        if(x>35) ws=R0;
        
        if(x==-1){
                m=icontrol.m;
                w=icontrol.w;
        }else{
                while(p[i].x!=-1 && i<15){
                        head+=p[i++].vector;
                }
                head/=i;
                if(i==10){
                        while(p[i].x!=-1 && i<20){
                                trail+=p[i++].vector;
                                j++;
                        }
                trail=trail/j;
                }
                        
        
       
        
        switch (trail){
                case 0:
                        m=S1;
                        break;
                case 1:
                case -1:
                        m=S1-SLOW;
                        break;
                case 2:
                case -2:
                case 3:
                case -3:
                        m=S1-2*SLOW;
                        break;
                default:
                        m=S1-3*SLOW;
        }
        motor_ctrl(m,1);

        switch(head){
                case 0:
                        m=S1;
                      //  w=icontrol.w;
                        break;
                case 1:
                        m=icontrol.m;
                        w=L1;
                        break;
                case -1:
                        m=icontrol.m;
                        w=R1;
                        break;
                case 2:
                case 3:
                        m=icontrol.m-2*SLOW;
                        w=L2;
                        break;
                case -2:
                case -3:
                        m=icontrol.m-2*SLOW;
                        w=R2;
                        break;
                case -4:
                case -5:
                case -6:
                case -7:
                case -8:
                case -9:
                case -10:
                case -11:
                case -12:
                        m=S0;
                        w=L3;
                        break;
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                        m=S0;
                        w=R3;
                        break;
                default:
                        m=S0;
                        w=MID;
        }
         }
         
        motor_ctrl(m,1);
        if(w<MID && w>ws) w=ws;
        if(w>MID && w<ws) w=ws;
        
        wplus_ctrl(w);
        icontrol.m=m;
        icontrol.w=w;
         
                
        return 0;
}