#ifndef BOARD_H
#define BOARD_H

#include "includes.h"
#include "image.h"


#define METER   3200

#define LED4 PORTB_PB4
#define LED3 PORTB_PB5
#define LED2 PORTB_PB6
#define LED1 PORTB_PB7

#define KEY1 PORTE_PE6
#define KEY2 PORTE_PE5
///#define KEY3 PORTE_PE6
//#define KEY4 PORTE_PE7

#define D0 PORTA_PA0
#define D1 PORTA_PA1
#define D2 PORTA_PA2
#define D3 PORTA_PA3
#define D4 PORTA_PA4
#define D5 PORTA_PA5
#define D6 PORTA_PA6
#define D7 PORTA_PA7

#define FIFO_REN        PORTB_PB0
#define FIFO_RCLK       PORTB_PB1
#define FIFO_OE         PORTB_PB2
        
#define ADC_OE          PORTB_PB3
        
#define FIFO_RS         PTT_PTT4
#define FIFO_WEN        PTT_PTT5
#define FIFO_EF         PTT_PTT6

#define ARM_RDY         PTM_PTM0
#define ARM_MODE        PTM_PTM5
#define ARM_CLK         PTM_PTM3
#define ARM_RCVD        PTM_PTM2

#define DATA_DDR        PTS_PTS3



extern unsigned char image_data[VER][HOR];


/*  **************************************************
 *  Board declared global variables .
 *  PAY ATTENTION to define each one at modified file.
 *  **************************************************/
/*
extern  byte LINECNT;		//Count line numbers while IRQ is enable
extern  byte FRAMECNT;		//Count frame numbers while PJINT is enable
extern  byte LINEACH;
extern  byte CNTFLAG;
extern  byte LOFCNT;      //Count each LINEACH capture one line
extern	int DR;
extern  int SPEED;
extern byte data[LINE][PIEX];


extern	byte	EVNB;		// Environment Brightness
extern  bool    KNOCK;       // Task communicating

struct ctrl_data{
	INT16U LOR;
	INT16U WPLUS;
	INT16U MOTOR;
	INT16U RVS;                        // Control motor reverse running
};

struct mod_data{
	byte MOD;	// Trace Module. LINE:0; 'S'route:1; 'U'route:2;
	byte STA;	// Route stage. Basically 3 Stage will be implemented. 1: head into; 2: in middle; 3: head out
        byte RQR;       // Module change require. Almost occured from 'S' 'U' to LINE
        
	int CNT;	// Trace central offset in LINE
	int TOP;	// Trace top boundary in LINE
	byte LFT;	// Trace left boundary in PIEX
	byte RIT;	// Trace right boundary in PIEX
};

struct pid_data{
        int kp;
        int ki;
        int kd;
};
*/

void delay(int );
void dvrinit(void);
void transchar(char *);
void transchar_n(char *);
int transdbg(char *buff, int size);
/*
void envb(void);
void ctrl_pad(void);
void data_proc(void);
void ctrl_0(void);
void ctrl_1(void);
void ctrl_2(void);
int feedback(int a, int b, struct pid_data pid);
int speed_acq(void);
*/
#endif
