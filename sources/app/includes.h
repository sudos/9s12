/*
*********************************************************************************************************
*                                                uC/OS-II
*                                          The Real-Time Kernel
*
*                              (c) Copyright 1992-2005, Micrium, Weston, FL
*                                           All Rights Reserved
*
*                                           MASTER INCLUDE FILE
*********************************************************************************************************
*/

#ifndef  INCLUDES_H
#define  INCLUDES_H



/* Including needed modules to compile this module/procedure */
#include "Cpu.h"
#include "Events.h"
#include "PJINT.h"
#include "IRQ.h"
#include "LED.h"
#include "DATA.h"

#include "ADCCLK.h"
#include "ENCDR.h"
/* Include shared modules, which are used for whole project */
#include "PE_Types.h"
#include "PE_Error.h"
#include "PE_Const.h"
#include "IO_Map.h"
#include "PWM1.h"
#include "PWM2.h"


//////////////////////////////////////////////////////////////////////

#include  <ucos_ii.h>
#include "board.h"

#include "image.h"
#include "control.h"
#include "image_prc.h"

#endif                                                                  /* End of file                                              */

