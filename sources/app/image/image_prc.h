#ifndef IMAGE_PRC_H
#define IMAGE_PRC_H

#define DEBUG

#ifdef DEBUG
	#define VER 80
	#define HOR 70
#else
	#include "image.h"
#endif



struct track{
	//int vector;
		int x;
		int y;
};

#ifdef DEBUG
int image_prc(unsigned char data[VER][HOR], int v, int h, unsigned char *pdata);
#else
int image_prc(unsigned char data[VER][HOR], int v, int h, struct track *itrack);
#endif

int image_head(unsigned char data[VER][HOR], int v, int h,struct track *itrack);

int image_body(unsigned char data[VER][HOR],  int v, int h, char start,struct track *itrack);


int image_trail(unsigned char data[VER][HOR], int v, int h, int offset,struct track *itrack);

int image_Fline(unsigned char data[VER][HOR], int v, int h, int offset, struct track *itrack);

int image_filter(struct track *itrack);
#endif
