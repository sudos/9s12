/** ###################################################################
 **     Filename  : Project.C
 **     Project   : Project
 **     Processor : MC9S12XS128MAA
 **     Version   : Driver 01.13
 **     Compiler  : CodeWarrior HCS12X C Compiler
 **     Date/Time : 4/15/2010, 4:42 PM
 **     Abstract  :
 **         Main module.
 **         This module contains user's application code.
 **     Settings  :
 **     Contents  :
 **         No public methods
 **
 **     (c) Copyright UNIS, a.s. 1997-2008
 **     UNIS, a.s.
 **     Jundrovska 33
 **     624 00 Brno
 **     Czech Republic
 **     http      : www.processorexpert.com
 **     mail      : info@processorexpert.com
 ** ###################################################################*/
/* MODULE Project */

/* Including needed modules to compile this module/procedure */
#include "Cpu.h"
#include "Events.h"
#include "PJINT.h"
#include "IRQ.h"
#include "LED.h"
#include "DATA.h"
#include "ADCCLK.h"
#include "KEY2.h"
#include "WPLUS.h"
#include "PWM1.h"
#include "PWM2.h"
#include "ENCDR.h"
#include "EF.h"
#include "SCI.h"
/* Include shared modules, which are used for whole project */
#include "PE_Types.h"
#include "PE_Error.h"
#include "PE_Const.h"
#include "IO_Map.h"

#include "board.h"
#include "image.h"



unsigned char image_data[VER][HOR];

extern unsigned char buff[1000];
unsigned char buff[1000]={0};

void main(void)
{
	/* Write your local variable definition here */

	/*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/

	INT32U ct=0;
	unsigned int i=0,j=0;

	char c=75;	        


	PE_low_level_init(); 

	//	__EI();       // Togger Enable or Disable Interrupt before aheading to loop

	/* Specify which interrupt(s) not used to Disable it explicitly */

	IRQ_D();		// Disable IRQ

	dvrinit();          //   Drivers init

	//	__EI();
	delay(1000);

	//	PJINT_E();

	LED1=0;
	LED2=0;
	LED3=0;
	LED4=0;
	/* Implement functional code in while(TRUE)  */
	//	ADCCLK_Disable ();

	i=0;
	j=0;

	while(TRUE){

		LED4=0;

		LED1=1;
		delay(500);
		for(i=0; i<2400; i++){
			buff[i]=j++;
			if(j>255) j=0;
		}


		delay(3000);
		sendb();



		while(1){
		/*

			if(!KEY2) {
#ifndef SENDER
#define SENDER
#endif
			}
                 */
			if(KEY1){
				delay(1000);

				while(1){

					image_cap(image_data);
					LED1=1;
					image_prc(image_data,VER,HOR);
                                        LED1=0;
					send(image_data,buff,5600,1000);

					

				}
			}
		}



		ct++;

	}



  /*** Processor Expert end of main routine. DON'T MODIFY THIS CODE!!! ***/
  for(;;){}
  /*** Processor Expert end of main routine. DON'T WRITE CODE BELOW!!! ***/
	/*** Processor Expert end of main routine. DON'T MODIFY THIS CODE!!! ***/
	for(;;){}
	/*** Processor Expert end of main routine. DON'T WRITE CODE BELOW!!! ***/
} /*** End of main routine. DO NOT MODIFY THIS TEXT!!! ***/

/* END Project */
/*
 ** ###################################################################
 **
 **     This file was created by UNIS Processor Expert 3.00 [04.12]
 **     for the Freescale HCS12X series of microcontrollers.
 **
 ** ###################################################################
 */
