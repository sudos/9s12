#include "includes.h"

static OS_STK DATA_CapStk[DATA_Cap_STK_SIZE];
static OS_STK MOTOR_CtrlStk[MOTOR_Ctrl_STK_SIZE];

static void DATA_Cap(void);
static void MOTOR_Ctrl(void);

int lmain(void){

	INT8U err;
//	PE_low_level_init();          // Init board ,in default all Interrupts are Enabled
	//__DI();                       // Disable all interrupt. Enable it after OS init

	OSInit();
	OSTaskCreateExt(DATA_Cap,
			(void *)0,
			(OS_STK *)&DATA_CapStk[DATA_Cap_STK_SIZE - 1],
			DATA_Cap_PRIO,
			DATA_Cap_PRIO,
			(OS_STK *)&DATA_CapStk[0],
			DATA_Cap_STK_SIZE,
			(void *)0,
			OS_TASK_OPT_STK_CHK | OS_TASK_OPT_STK_CLR);
/*
	OSTaskCreateExt(MOTOR_Ctrl,
			(void *)0,
			(OS_STK *)&MOTOR_CtrlStk[MOTOR_Ctrl_STK_SIZE - 1],
			MOTOR_Ctrl_PRIO,
			MOTOR_Ctrl_PRIO,
			(OS_STK *)&MOTOR_CtrlStk[0],
			MOTOR_Ctrl_STK_SIZE,
			(void *)0,
			OS_TASK_OPT_STK_CHK | OS_TASK_OPT_STK_CLR);

*/
	//	OSTaskNameSet(LED_Show_PRIO,(char *)"LED Show",&err);

	OSStart();

}

void DATA_Cap(void){

	INT32U ct=0;
	unsigned int i=0,j;
	char image_data[VER][HOR];
	
	PE_low_level_init(); 

	__EI();       // Togger Enable or Disable Interrupt before aheading to loop

	/* Specify which interrupt(s) not used to Disable it explicitly */

	IRQ_D();		// Disable IRQ

	dvrinit();          //   Drivers init
	delay(1000);

		PJINT_E();
	/* Implement functional code in while(TRUE)  */

	while(TRUE){

                while(PJINT_S);
                
                PORTB_PB6=0;
                image_cap(image_data);
                PORTB_PB6=1;
      /*          
		CNTFLAG=1;
		LINECNT=0;
		IRQ_Enable();
		//	while(!CNTFLAG);            // Spin-Lock
		PORTB=0XE0;
		while(CNTFLAG);
		IRQ_Disable();            // You may wanna clean it somtime
		PORTB=0XF0;

	//	data_proc();              // Processing data before ahead to control pad
	//	mode_g();		  // Generating Module

	//	transchar(); 
	
	*/   /*
		if(!KEY2) {
			delay(1000);
			PORTB=~PORTB;
			transchar_n(image_data); 
		}
		if(!KEY1){
			delay(1000);
			PORTB=~PORTB;
			transchar_n(image_data);
		}
	*/	
		PJINT_S = 1;
        
//		KNOCK=1;
//		OSTimeDly(2);
 
	}

}

void DATA_proc(void){

	INT32U ct=0;
	INT8U i,j;

	PJINT_E();

	__EI();

	while(TRUE){
	
	//	KNOCK=0;
		/* Add code here */

	//	while(!KNOCK){
	//		ct=OSTime;
	//		ctrl_pad();
	//	}

	}
}

void MOTOR_Ctrl(void){

	INT32U ct=0;
	INT8U i,j;

	PJINT_E();

	__EI();

	while(TRUE){

	}
}
