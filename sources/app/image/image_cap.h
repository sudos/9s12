#ifndef IMAGE_CAP_H
#define IMAGE_CAP_H

#include "includes.h"
#include "image.h"

#define H_OFFSET 80
#define V_OFFSET 8000


struct track{
	int vector;
	int x;
	int y;
};

int image_cap(char image[VER][HOR]) ;

#endif