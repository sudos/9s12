#include "image_prc.h"



unsigned char black=255;
unsigned char white =0;


#ifdef DEBUG
int image_prc(unsigned char data[VER][HOR], int v, int h,unsigned char *pdata){
	struct track itrack[100]={{0,0}};
#else
	int image_prc(unsigned char data[VER][HOR], int v, int h, struct track *itrack){
#endif
		int t=0;
		int i=0,j=0;
		int ix,iy;
		int tt=0;	

#ifdef	DEBUG

		for(i=0; i<5600; i++)
			*(pdata+i)=255;

		for(i=0;i<8; i++){
			for(j=0; j<70; j++)
				*(pdata+i*700+j)=200;

		}
#endif


		t=0;
		t=image_head(data, v, h,itrack);

		if(t<2){
			if(t==-1){
				itrack[0].x = -1;
				itrack[0].y = -1;
			}else{
				itrack[t].x = -1;
				itrack[t].y = -1;
			}
			return 0;
		}else{
			t=image_trail(data,v,h,t,itrack);
			itrack[t].x=-1;
			itrack[t].y=-1;


			tt=image_Fline(data,v,h,t,itrack);
			if(tt==1){
				itrack[t].x=0;
			}else{

				tt=image_crossline(data,v,h,t,itrack);

				if(tt==1){
					itrack[t].x=1;
				}
			}
		}

#ifdef	 DEBUG
		for(i=1; i<t; i++){
			ix=itrack[i].x;
			iy=itrack[i].y;
			*(pdata+(iy)*h+ix)=0;
		}

		ix=itrack[t-1].x;
		iy=itrack[t-1].y;
		if(tt==1) data[0][3]=255;
#endif			

		return tt;

	}



	int image_head(unsigned char data[VER][HOR], int v, int h,struct track *itrack){

		int temp;
		int i=0;
		int pf=0,nf=0,j=0,k=0,t=0,offset=0,f=0,po=0,flag=0;
		int left=h, right=0,it=0,local=0,crital=0,scale=0;
		unsigned char apot[5]={0};
		unsigned char a=0,b=0,c=0;


		it=0;
		local=12;
		offset=80;


		for(po=v-1; po>40; po--){
			flag=0;
			temp = h*(po-1);
			pf=0;
			left=h;
			right =0;
			nf=0;

			for(i=1; i< h-3; i++){
				if(i<h-3 && i>0){
					j=(unsigned char)data[po][i] ;
					k=(unsigned char)data[po][i+3] ;
					if(j-k>offset && flag==0){
						//data[po][i]=255;

						left=i+3;
						i=i;
						flag=1;
					}
					if(k-j>offset && flag==1){
						right=i;

						if(right-left<20){						//Add start line searching here

							itrack[it].x = i-1;
							itrack[it].y = po;
							local=i;
							crital=0;

							goto body;							// lol Found it!
						}else {
							flag=0;
						}
					}
				}
			}				
			po--;
		}	
		return 0;

body:	
		local=itrack[it].x;
		po=itrack[it].y;
		offset=100;                                               //80
		scale=15;
		crital=0;

		for(;po>1; po--){
			flag=0;
			pf=0;
			left=h;
			right =0;
			nf=0;
			a=0;
			if(po==40) {
				offset=60;
				scale=15;
			}

			if(po==35) offset=80;             //60
			if(po==25) offset=60;             //40
			if(po== 15) offset=40;
			if(po<=40 && it==0) return 0;
			for(i=local-scale; i< local+scale; i++){
				if(i<h-2 && i>0){
					j=(unsigned char)data[po][i] ;
					k=(unsigned char)data[po][i+4] ;
					if(j-k>offset && flag==0){
						left=i;

						i=i;
						flag=1;
					}
					if(k-j>offset && flag==1){
						right=i;	
						if(a<5) apot[a]=(i+left+6)/2;			// apot[] size is 5

						a++;
						flag=0;
					}

				}
			}
			if(a==0 || a==4) {
				crital++;
			}
			else{											// Store nearest val in apot[4]
				temp=255;
				for(b=0; b<a; b++){
					if(apot[b]>itrack[it].x){
						c=apot[b]-itrack[it].x;
						if(temp>c) {
							temp=c;
							apot[4]=apot[b];
						}
					}else{
						c=itrack[it].x-apot[b];
						if(temp>c) {
							temp=c;
							apot[4]=apot[b];
						}
					}
				}
				if(temp==255){
					it++;
					itrack[it].x=itrack[it-1].x;
					itrack[it].y=po;
					crital++;
				}else{
					it++;
					itrack[it].x=apot[4];
					itrack[it].y=po;
					local=2*itrack[it].x-itrack[it-1].x;
					crital=0;
				}
			}					

			if(crital>6) {
				return it;
			}
		}

		return it;
	}

	int image_trail(unsigned char data[VER][HOR], int v, int h, int offset,struct track *itrack){
		int i=0;
		unsigned char j=0,k=0;
		int xo,yo,vo;
		int temp;
		int step;
		int crital=0,flag=0;
		int scale=0;

		xo=itrack[offset].x;
		yo=itrack[offset].y;
		temp=itrack[offset-2].x;

		if(yo<2) return offset;
		if(xo-temp >-3 && xo-temp<3) return offset;

		if(yo>10) {
			step=40;
			scale=3;
		}

		if(yo>20) {
			step = 40;
			scale=3;
		}
		if(yo>40) {
			step=80;
			scale=5;
		}

		if(xo-temp<-1){
			while(xo>0 && offset<99){

				flag=0;
				for(i=scale; i>0-scale; i--){
					if(yo+i-2>1){
						j=(unsigned char)data[yo+i][xo];
						k=(unsigned char)data[yo+i-2][xo];
						if(j-k>step){
							offset++;
							itrack[offset].x=xo;
							itrack[offset].y=yo+i-2;
							yo=itrack[offset].y;
							flag=1;
							break;
						}

					}



				}
				if(flag==0) {				
					crital++;
					if(crital>2){
						return offset;
					}
				}

				xo--;
				xo--;
			}
		}else{
			while(xo<h-3 && offset<99){

				flag=0;
				for(i=scale; i>0-scale; i--){
					if(yo+i-2>1){
						j=(unsigned char)data[yo+i][xo];
						k=(unsigned char)data[yo+i-2][xo];
						if(j-k>step){
							offset++;
							itrack[offset].x=xo;
							itrack[offset].y=yo+i-2;
							yo=itrack[offset].y;
							flag=1;
							//break;
						}

					}

				}

				if(flag==0){
					crital++;
					if(crital>2){
						return offset;
					}
				}


				xo++;
				xo++;
			}
		}		
		return offset;
	}

	int image_Fline(unsigned char data[VER][HOR], int v, int h, int offset, struct track *itrack){

		int step=0;
		int i=0;
		int p=0,n=0,m=0,j=0,k=0,t=0,f=0,po=0,flag=0;
		int left=h, right=0,it=0,local=0,crital=0,scale=0,xo=0,yo=0;


		it=1;
		local=12;
		step=60;

		xo=(char)itrack[offset-1].x;
		p=(char)itrack[offset-1].y;

		flag=xo-(char)itrack[2].x;

		if(p>80  || itrack[2].x>77 || itrack[2].x<2){		// Limitation

			return 0;
		}

		f=2;
		yo=(char)itrack[f].y;
		xo=(char)itrack[f].x;
		while(yo>30 && f<50){

			if(yo==-1) return 0;
			else{

				for(i=2; i<15;i++){
					if(i+xo+2<h-1){
						j=(unsigned char)data[yo][i+xo];
						k=(unsigned char)data[yo][i+xo+2];
						if(j-k>step){

							for(n=2; n<15; n++){
								if(i+xo+2+n <h){
									j=(unsigned char) data[yo][i+xo+2+n];
									k=(unsigned char) data[yo+3][i+xo+2+n];

									if(k-j<step) break;
								}
							}
							if(n>12 && n<30) return 1;

						}
					}
				}

				for(i=2; i<15;i++){
					if(xo-i-2>0){
						j=(unsigned char)data[yo][xo-i];
						k=(unsigned char)data[yo][xo-i-2];

						if(j-k>step){

							for(n=0; n<25; n++){
								if(xo-i-2-n >0){
									j=(unsigned char) data[yo][xo-i-2-n];
									k=(unsigned char) data[yo+3][xo-i-2-n];

									if(k-j<step) break;
								}
							}
							if(n>12 && n<25) 
								return 1;
						}
					}
				}
			}


			f++;
			yo=(char)itrack[f].y;
			xo=(char)itrack[f].x;
		}

		return 0;	
	}


	int image_crossline(unsigned char data[VER][HOR],int v, int h, int offset, struct track *itrack){

		int step=0;
		int i=0;
		int p=0,n=0,m=0,j=0,k=0,t=0,f=0,po=0,flag=0;
		int left=h, right=0,it=0,local=0,crital=0,scale=0,xo=0,yo=0;


		it=1;
		local=12;
		step=60;

		xo=(char)itrack[offset-1].x;
		p=(char)itrack[offset-1].y;

		flag=xo-(char)itrack[2].x;

		if(p>60  || itrack[2].x>70 || itrack[2].x<10){		// Limitation
			return 0;
		}

		f=2;
		yo=(char)itrack[f].y;
		xo=(char)itrack[f].x;

		for(i=3; i<offset-1; i++){
			if((itrack[i-1].y-itrack[i].y)>4) break;
		}

		if(i==offset-1) return 0;

		if(itrack[i].y>40) return 1;
		else return 0;	
	}
